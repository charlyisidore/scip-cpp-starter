/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "vardata_xyz.hpp"

SCIP_RETCODE VardataXyz::scip_delorig(SCIP *scip, SCIP_VAR *var)
{
    return scip::ObjVardata::scip_delorig(scip, var);
}

SCIP_RETCODE VardataXyz::scip_trans(SCIP *scip,
                                    SCIP_VAR *var,
                                    scip::ObjVardata **objvardata,
                                    SCIP_Bool *deleteobject)
{
    return scip::ObjVardata::scip_trans(scip, var, objvardata, deleteobject);
}

SCIP_RETCODE VardataXyz::scip_deltrans(SCIP *scip, SCIP_VAR *var)
{
    return scip::ObjVardata::scip_deltrans(scip, var);
}

SCIP_RETCODE VardataXyz::scip_copy(SCIP *scip,
                                   SCIP *sourcescip,
                                   SCIP_VAR *sourcevar,
                                   SCIP_HASHMAP *varmap,
                                   SCIP_HASHMAP *consmap,
                                   SCIP_VAR *targetvar,
                                   scip::ObjVardata **objvardata,
                                   SCIP_RESULT *result)
{
    return scip::ObjVardata::scip_copy(scip,
                                       sourcescip,
                                       sourcevar,
                                       varmap,
                                       consmap,
                                       targetvar,
                                       objvardata,
                                       result);
}