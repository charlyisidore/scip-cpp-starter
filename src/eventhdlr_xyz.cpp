/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "eventhdlr_xyz.hpp"

const char *EventhdlrXyz::Properties::Name = "xyz";
const char *EventhdlrXyz::Properties::Desc = "event handler for xyz event";

EventhdlrXyz::EventhdlrXyz(SCIP *scip)
    : scip::ObjEventhdlr(scip, Properties::Name, Properties::Desc)
{
}

SCIP_RETCODE EventhdlrXyz::scip_free(SCIP *scip, SCIP_EVENTHDLR *eventhdlr)
{
    return scip::ObjEventhdlr::scip_free(scip, eventhdlr);
}

SCIP_RETCODE EventhdlrXyz::scip_init(SCIP *scip, SCIP_EVENTHDLR *eventhdlr)
{
    return scip::ObjEventhdlr::scip_init(scip, eventhdlr);
}

SCIP_RETCODE EventhdlrXyz::scip_exit(SCIP *scip, SCIP_EVENTHDLR *eventhdlr)
{
    return scip::ObjEventhdlr::scip_exit(scip, eventhdlr);
}

SCIP_RETCODE EventhdlrXyz::scip_initsol(SCIP *scip, SCIP_EVENTHDLR *eventhdlr)
{
    return scip::ObjEventhdlr::scip_initsol(scip, eventhdlr);
}

SCIP_RETCODE EventhdlrXyz::scip_exitsol(SCIP *scip, SCIP_EVENTHDLR *eventhdlr)
{
    return scip::ObjEventhdlr::scip_exitsol(scip, eventhdlr);
}

SCIP_RETCODE EventhdlrXyz::scip_delete(SCIP *scip,
                                       SCIP_EVENTHDLR *eventhdlr,
                                       SCIP_EVENTDATA **eventdata)
{
    return scip::ObjEventhdlr::scip_delete(scip, eventhdlr, eventdata);
}

SCIP_RETCODE EventhdlrXyz::scip_exec([[maybe_unused]] SCIP *scip,
                                     [[maybe_unused]] SCIP_EVENTHDLR *eventhdlr,
                                     [[maybe_unused]] SCIP_EVENT *event,
                                     [[maybe_unused]] SCIP_EVENTDATA *eventdata)
{
    return SCIP_OKAY;
}