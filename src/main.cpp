/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "branchrule_xyz.hpp"
#include "conshdlr_xyz.hpp"
#include "eventhdlr_xyz.hpp"
#include "heur_xyz.hpp"
#include "messagehdlr_xyz.hpp"
#include "presol_xyz.hpp"
#include "pricer_xyz.hpp"
#include "reader_xyz.hpp"
#include "relax_xyz.hpp"
#include "table_xyz.hpp"
#include <cstdlib>
#include <scip/scip.h>
#include <scip/scipdefplugins.h>

static SCIP_RETCODE run(int argc, char **argv)
{
    SCIP *scip;
    SCIP_MESSAGEHDLR *messagehdlr;

    SCIP_CALL(SCIPcreate(&scip));

    SCIPenableDebugSol(scip);

    SCIP_CALL(SCIPcreateObjMessagehdlr(&messagehdlr, new MessagehdlrXyz(), true));
    SCIP_CALL(SCIPsetMessagehdlr(scip, messagehdlr));

    SCIP_CALL(SCIPincludeObjBranchrule(scip, new BranchruleXyz(scip), true));
    SCIP_CALL(SCIPincludeObjConshdlr(scip, new ConshdlrXyz(scip), true));
    SCIP_CALL(SCIPincludeObjEventhdlr(scip, new EventhdlrXyz(scip), true));
    SCIP_CALL(SCIPincludeObjHeur(scip, new HeurXyz(scip), true));
    SCIP_CALL(SCIPincludeObjPresol(scip, new PresolXyz(scip), true));
    SCIP_CALL(SCIPincludeObjPricer(scip, new PricerXyz(scip), true));
    SCIP_CALL(SCIPincludeObjReader(scip, new ReaderXyz(scip), true));
    SCIP_CALL(SCIPincludeObjRelax(scip, new RelaxXyz(scip), true));
    SCIP_CALL(SCIPincludeObjTable(scip, new TableXyz(scip), true));

    SCIP_CALL(SCIPincludeDefaultPlugins(scip));

    SCIP_CALL(SCIPprocessShellArguments(scip, argc, argv, nullptr));

    SCIP_CALL(SCIPfree(&scip));

    BMScheckEmptyMemory();

    return SCIP_OKAY;
}

int main(int argc, char **argv)
{
    SCIP_RETCODE retcode = run(argc, argv);

    if (retcode != SCIP_OKAY)
    {
        SCIPprintError(retcode);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}