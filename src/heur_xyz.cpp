/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "heur_xyz.hpp"

const char *HeurXyz::Properties::Name = "xyz";
const char *HeurXyz::Properties::Desc = "xyz primal heuristic";
const char HeurXyz::Properties::Dispchar = '?';
const int HeurXyz::Properties::Priority = 0;
const int HeurXyz::Properties::Freq = 1;
const int HeurXyz::Properties::Freqofs = 0;
const int HeurXyz::Properties::Maxdepth = -1;
const SCIP_HEURTIMING HeurXyz::Properties::Timingmask = SCIP_HEURTIMING_AFTERNODE;
const SCIP_Bool HeurXyz::Properties::Usesubscip = false;

HeurXyz::HeurXyz(SCIP *scip)
    : scip::ObjHeur(scip,
                    Properties::Name,
                    Properties::Desc,
                    Properties::Dispchar,
                    Properties::Priority,
                    Properties::Freq,
                    Properties::Freqofs,
                    Properties::Maxdepth,
                    Properties::Timingmask,
                    Properties::Usesubscip)
{
}

SCIP_RETCODE HeurXyz::scip_free(SCIP *scip, SCIP_HEUR *heur)
{
    return scip::ObjHeur::scip_free(scip, heur);
}

SCIP_RETCODE HeurXyz::scip_init(SCIP *scip, SCIP_HEUR *heur)
{
    return scip::ObjHeur::scip_init(scip, heur);
}

SCIP_RETCODE HeurXyz::scip_exit(SCIP *scip, SCIP_HEUR *heur)
{
    return scip::ObjHeur::scip_exit(scip, heur);
}

SCIP_RETCODE HeurXyz::scip_initsol(SCIP *scip, SCIP_HEUR *heur)
{
    return scip::ObjHeur::scip_initsol(scip, heur);
}

SCIP_RETCODE HeurXyz::scip_exitsol(SCIP *scip, SCIP_HEUR *heur)
{
    return scip::ObjHeur::scip_exitsol(scip, heur);
}

SCIP_RETCODE HeurXyz::scip_exec([[maybe_unused]] SCIP *scip,
                                [[maybe_unused]] SCIP_HEUR *heur,
                                [[maybe_unused]] SCIP_HEURTIMING heurtiming,
                                [[maybe_unused]] SCIP_Bool nodeinfeasible,
                                [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}
