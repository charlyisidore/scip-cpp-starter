/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "messagehdlr_xyz.hpp"
#include <cstdio>

const SCIP_Bool MessagehdlrXyz::Properties::Bufferedoutput = true;

MessagehdlrXyz::MessagehdlrXyz()
    : scip::ObjMessagehdlr(Properties::Bufferedoutput)
{
}

void MessagehdlrXyz::scip_error([[maybe_unused]] SCIP_MESSAGEHDLR *messagehdlr,
                                FILE *file,
                                const char *msg)
{
    std::fputs(msg, file ? file : stdout);
}

void MessagehdlrXyz::scip_warning([[maybe_unused]] SCIP_MESSAGEHDLR *messagehdlr,
                                  FILE *file,
                                  const char *msg)
{
    std::fputs(msg, file ? file : stdout);
}

void MessagehdlrXyz::scip_dialog([[maybe_unused]] SCIP_MESSAGEHDLR *messagehdlr,
                                 FILE *file,
                                 const char *msg)
{
    std::fputs(msg, file ? file : stdout);
}

void MessagehdlrXyz::scip_info([[maybe_unused]] SCIP_MESSAGEHDLR *messagehdlr,
                               FILE *file,
                               const char *msg)
{
    std::fputs(msg, file ? file : stdout);
}

SCIP_RETCODE MessagehdlrXyz::scip_free(SCIP_MESSAGEHDLR *messagehdlr)
{
    return scip::ObjMessagehdlr::scip_free(messagehdlr);
}