/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "reader_xyz.hpp"

const char *ReaderXyz::Properties::Name = "xyzreader";
const char *ReaderXyz::Properties::Desc = "xyz file reader";
const char *ReaderXyz::Properties::Extension = "xyz";

ReaderXyz::ReaderXyz(SCIP *scip)
    : scip::ObjReader(scip,
                      Properties::Name,
                      Properties::Desc,
                      Properties::Extension)
{
}

SCIP_RETCODE ReaderXyz::scip_free(SCIP *scip, SCIP_READER *reader)
{
    return scip::ObjReader::scip_free(scip, reader);
}

SCIP_RETCODE ReaderXyz::scip_read(SCIP *scip,
                                  SCIP_READER *reader,
                                  const char *filename,
                                  SCIP_RESULT *result)
{
    return scip::ObjReader::scip_read(scip, reader, filename, result);
}

SCIP_RETCODE ReaderXyz::scip_write(SCIP *scip,
                                   SCIP_READER *reader,
                                   FILE *file,
                                   const char *name,
                                   SCIP_PROBDATA *probdata,
                                   SCIP_Bool transformed,
                                   SCIP_OBJSENSE objsense,
                                   SCIP_Real objscale,
                                   SCIP_Real objoffset,
                                   SCIP_VAR **vars,
                                   int nvars,
                                   int nbinvars,
                                   int nintvars,
                                   int nimplvars,
                                   int ncontvars,
                                   SCIP_VAR **fixedvars,
                                   int nfixedvars,
                                   int startnvars,
                                   SCIP_CONS **conss,
                                   int nconss,
                                   int maxnconss,
                                   int startnconss,
                                   SCIP_Bool genericnames,
                                   SCIP_RESULT *result)
{
    return scip::ObjReader::scip_write(scip,
                                       reader,
                                       file,
                                       name,
                                       probdata,
                                       transformed,
                                       objsense,
                                       objscale,
                                       objoffset,
                                       vars,
                                       nvars,
                                       nbinvars,
                                       nintvars,
                                       nimplvars,
                                       ncontvars,
                                       fixedvars,
                                       nfixedvars,
                                       startnvars,
                                       conss,
                                       nconss,
                                       maxnconss,
                                       startnconss,
                                       genericnames,
                                       result);
}