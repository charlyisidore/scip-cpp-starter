/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "table_xyz.hpp"

const char *TableXyz::Properties::Name = "xyz";
const char *TableXyz::Properties::Desc = "xyz statistics table";
const int TableXyz::Properties::Position = -1000;
const SCIP_STAGE TableXyz::Properties::Earlieststage = SCIP_STAGE_INIT;

TableXyz::TableXyz(SCIP *scip)
    : scip::ObjTable(scip,
                     Properties::Name,
                     Properties::Desc,
                     Properties::Position,
                     Properties::Earlieststage)
{
}

SCIP_RETCODE TableXyz::scip_free(SCIP *scip, SCIP_TABLE *table)
{
    return scip::ObjTable::scip_free(scip, table);
}

SCIP_RETCODE TableXyz::scip_init(SCIP *scip, SCIP_TABLE *table)
{
    return scip::ObjTable::scip_init(scip, table);
}

SCIP_RETCODE TableXyz::scip_exit(SCIP *scip, SCIP_TABLE *table)
{
    return scip::ObjTable::scip_exit(scip, table);
}

SCIP_RETCODE TableXyz::scip_initsol(SCIP *scip, SCIP_TABLE *table)
{
    return scip::ObjTable::scip_initsol(scip, table);
}

SCIP_RETCODE TableXyz::scip_exitsol(SCIP *scip, SCIP_TABLE *table)
{
    return scip::ObjTable::scip_exitsol(scip, table);
}

SCIP_RETCODE TableXyz::scip_output([[maybe_unused]] SCIP *scip,
                                   [[maybe_unused]] SCIP_TABLE *table,
                                   [[maybe_unused]] FILE *file)
{
    return SCIP_OKAY;
}