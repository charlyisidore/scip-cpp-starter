/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "probdata_xyz.hpp"

SCIP_RETCODE ProbdataXyz::scip_delorig(SCIP *scip)
{
    return scip::ObjProbData::scip_delorig(scip);
}

SCIP_RETCODE ProbdataXyz::scip_trans(SCIP *scip,
                                     scip::ObjProbData **objprobdata,
                                     SCIP_Bool *deleteobject)
{
    return scip::ObjProbData::scip_trans(scip, objprobdata, deleteobject);
}

SCIP_RETCODE ProbdataXyz::scip_deltrans(SCIP *scip)
{
    return scip::ObjProbData::scip_deltrans(scip);
}

SCIP_RETCODE ProbdataXyz::scip_initsol(SCIP *scip)
{
    return scip::ObjProbData::scip_initsol(scip);
}

SCIP_RETCODE ProbdataXyz::scip_exitsol(SCIP *scip, SCIP_Bool restart)
{
    return scip::ObjProbData::scip_exitsol(scip, restart);
}

SCIP_RETCODE ProbdataXyz::scip_copy(SCIP *scip,
                                    SCIP *sourcescip,
                                    SCIP_HASHMAP *varmap,
                                    SCIP_HASHMAP *consmap,
                                    scip::ObjProbData **objprobdata,
                                    SCIP_Bool global,
                                    SCIP_RESULT *result)
{
    return scip::ObjProbData::scip_copy(scip,
                                        sourcescip,
                                        varmap,
                                        consmap,
                                        objprobdata,
                                        global,
                                        result);
}