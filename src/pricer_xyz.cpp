/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pricer_xyz.hpp"

const char *PricerXyz::Properties::Name = "xyz";
const char *PricerXyz::Properties::Desc = "xyz variable pricer";
const int PricerXyz::Properties::Priority = 0;
const SCIP_Bool PricerXyz::Properties::Delay = true;

PricerXyz::PricerXyz(SCIP *scip)
    : scip::ObjPricer(scip,
                      Properties::Name,
                      Properties::Desc,
                      Properties::Priority,
                      Properties::Delay)
{
}

SCIP_RETCODE PricerXyz::scip_free(SCIP *scip, SCIP_PRICER *pricer)
{
    return scip::ObjPricer::scip_free(scip, pricer);
}

SCIP_RETCODE PricerXyz::scip_init(SCIP *scip, SCIP_PRICER *pricer)
{
    return scip::ObjPricer::scip_init(scip, pricer);
}

SCIP_RETCODE PricerXyz::scip_exit(SCIP *scip, SCIP_PRICER *pricer)
{
    return scip::ObjPricer::scip_exit(scip, pricer);
}

SCIP_RETCODE PricerXyz::scip_initsol(SCIP *scip, SCIP_PRICER *pricer)
{
    return scip::ObjPricer::scip_initsol(scip, pricer);
}

SCIP_RETCODE PricerXyz::scip_exitsol(SCIP *scip, SCIP_PRICER *pricer)
{
    return scip::ObjPricer::scip_exitsol(scip, pricer);
}

SCIP_RETCODE PricerXyz::scip_redcost([[maybe_unused]] SCIP *scip,
                                     [[maybe_unused]] SCIP_PRICER *pricer,
                                     [[maybe_unused]] SCIP_Real *lowerbound,
                                     [[maybe_unused]] SCIP_Bool *stopearly,
                                     [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}

SCIP_RETCODE PricerXyz::scip_farkas(SCIP *scip,
                                    SCIP_PRICER *pricer,
                                    SCIP_RESULT *result)
{
    return scip::ObjPricer::scip_farkas(scip, pricer, result);
}