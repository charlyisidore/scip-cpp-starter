/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "presol_xyz.hpp"

const char *PresolXyz::Properties::Name = "xyz";
const char *PresolXyz::Properties::Desc = "xyz presolver";
const int PresolXyz::Properties::Priority = 0;
const int PresolXyz::Properties::Maxrounds = -1;
const SCIP_PRESOLTIMING PresolXyz::Properties::Timing = SCIP_PRESOLTIMING_MEDIUM;

PresolXyz::PresolXyz(SCIP *scip)
    : scip::ObjPresol(scip,
                      Properties::Name,
                      Properties::Desc,
                      Properties::Priority,
                      Properties::Maxrounds,
                      Properties::Timing)
{
}

SCIP_RETCODE PresolXyz::scip_free(SCIP *scip, SCIP_PRESOL *presol)
{
    return scip::ObjPresol::scip_free(scip, presol);
}

SCIP_RETCODE PresolXyz::scip_init(SCIP *scip, SCIP_PRESOL *presol)
{
    return scip::ObjPresol::scip_init(scip, presol);
}

SCIP_RETCODE PresolXyz::scip_exit(SCIP *scip, SCIP_PRESOL *presol)
{
    return scip::ObjPresol::scip_exit(scip, presol);
}

SCIP_RETCODE PresolXyz::scip_initpre(SCIP *scip, SCIP_PRESOL *presol)
{
    return scip::ObjPresol::scip_initpre(scip, presol);
}

SCIP_RETCODE PresolXyz::scip_exitpre(SCIP *scip, SCIP_PRESOL *presol)
{
    return scip::ObjPresol::scip_exitpre(scip, presol);
}

SCIP_RETCODE PresolXyz::scip_exec([[maybe_unused]] SCIP *scip,
                                  [[maybe_unused]] SCIP_PRESOL *presol,
                                  [[maybe_unused]] int nrounds,
                                  [[maybe_unused]] SCIP_PRESOLTIMING presoltiming,
                                  [[maybe_unused]] int nnewfixedvars,
                                  [[maybe_unused]] int nnewaggrvars,
                                  [[maybe_unused]] int nnewchgvartypes,
                                  [[maybe_unused]] int nnewchgbds,
                                  [[maybe_unused]] int nnewholes,
                                  [[maybe_unused]] int nnewdelconss,
                                  [[maybe_unused]] int nnewaddconss,
                                  [[maybe_unused]] int nnewupgdconss,
                                  [[maybe_unused]] int nnewchgcoefs,
                                  [[maybe_unused]] int nnewchgsides,
                                  [[maybe_unused]] int *nfixedvars,
                                  [[maybe_unused]] int *naggrvars,
                                  [[maybe_unused]] int *nchgvartypes,
                                  [[maybe_unused]] int *nchgbds,
                                  [[maybe_unused]] int *naddholes,
                                  [[maybe_unused]] int *ndelconss,
                                  [[maybe_unused]] int *naddconss,
                                  [[maybe_unused]] int *nupgdconss,
                                  [[maybe_unused]] int *nchgcoefs,
                                  [[maybe_unused]] int *nchgsides,
                                  [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}
