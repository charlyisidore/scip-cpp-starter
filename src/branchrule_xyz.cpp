/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "branchrule_xyz.hpp"

const char *BranchruleXyz::Properties::Name = "xyz";
const char *BranchruleXyz::Properties::Desc = "xyz branching rule";
const int BranchruleXyz::Properties::Priority = 0;
const int BranchruleXyz::Properties::Maxdepth = -1;
const SCIP_Real BranchruleXyz::Properties::Maxbounddist = 1.;

BranchruleXyz::BranchruleXyz(SCIP *scip)
    : scip::ObjBranchrule(scip,
                          Properties::Name,
                          Properties::Desc,
                          Properties::Priority,
                          Properties::Maxdepth,
                          Properties::Maxbounddist)
{
}

SCIP_RETCODE BranchruleXyz::scip_free(SCIP *scip, SCIP_BRANCHRULE *branchrule)
{
    return scip::ObjBranchrule::scip_free(scip, branchrule);
}

SCIP_RETCODE BranchruleXyz::scip_init(SCIP *scip, SCIP_BRANCHRULE *branchrule)
{
    return scip::ObjBranchrule::scip_init(scip, branchrule);
}

SCIP_RETCODE BranchruleXyz::scip_exit(SCIP *scip, SCIP_BRANCHRULE *branchrule)
{
    return scip::ObjBranchrule::scip_exit(scip, branchrule);
}

SCIP_RETCODE BranchruleXyz::scip_initsol(SCIP *scip, SCIP_BRANCHRULE *branchrule)
{
    return scip::ObjBranchrule::scip_initsol(scip, branchrule);
}

SCIP_RETCODE BranchruleXyz::scip_exitsol(SCIP *scip, SCIP_BRANCHRULE *branchrule)
{
    return scip::ObjBranchrule::scip_exitsol(scip, branchrule);
}

SCIP_RETCODE BranchruleXyz::scip_execlp(SCIP *scip,
                                        SCIP_BRANCHRULE *branchrule,
                                        SCIP_Bool allowaddcons,
                                        SCIP_RESULT *result)
{
    return scip::ObjBranchrule::scip_execlp(scip,
                                            branchrule,
                                            allowaddcons,
                                            result);
}

SCIP_RETCODE BranchruleXyz::scip_execext(SCIP *scip,
                                         SCIP_BRANCHRULE *branchrule,
                                         SCIP_Bool allowaddcons,
                                         SCIP_RESULT *result)
{
    return scip::ObjBranchrule::scip_execext(scip,
                                             branchrule,
                                             allowaddcons,
                                             result);
}

SCIP_RETCODE BranchruleXyz::scip_execps(SCIP *scip,
                                        SCIP_BRANCHRULE *branchrule,
                                        SCIP_Bool allowaddcons,
                                        SCIP_RESULT *result)
{
    return scip::ObjBranchrule::scip_execps(scip,
                                            branchrule,
                                            allowaddcons,
                                            result);
}