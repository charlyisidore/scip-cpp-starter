/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "relax_xyz.hpp"

const char *RelaxXyz::Properties::Name = "xyz";
const char *RelaxXyz::Properties::Desc = "xyz relaxator";
const int RelaxXyz::Properties::Priority = 0;
const int RelaxXyz::Properties::Freq = 1;
const SCIP_Bool RelaxXyz::Properties::Includeslp = false;

RelaxXyz::RelaxXyz(SCIP *scip)
    : scip::ObjRelax(scip,
                     Properties::Name,
                     Properties::Desc,
                     Properties::Priority,
                     Properties::Freq,
                     Properties::Includeslp)
{
}

SCIP_RETCODE RelaxXyz::scip_free(SCIP *scip, SCIP_RELAX *relax)
{
    return scip::ObjRelax::scip_free(scip, relax);
}

SCIP_RETCODE RelaxXyz::scip_init(SCIP *scip, SCIP_RELAX *relax)
{
    return scip::ObjRelax::scip_init(scip, relax);
}

SCIP_RETCODE RelaxXyz::scip_exit(SCIP *scip, SCIP_RELAX *relax)
{
    return scip::ObjRelax::scip_exit(scip, relax);
}

SCIP_RETCODE RelaxXyz::scip_initsol(SCIP *scip, SCIP_RELAX *relax)
{
    return scip::ObjRelax::scip_initsol(scip, relax);
}

SCIP_RETCODE RelaxXyz::scip_exitsol(SCIP *scip, SCIP_RELAX *relax)
{
    return scip::ObjRelax::scip_exitsol(scip, relax);
}

SCIP_RETCODE RelaxXyz::scip_exec([[maybe_unused]] SCIP *scip,
                                 [[maybe_unused]] SCIP_RELAX *relax,
                                 [[maybe_unused]] SCIP_Real *lowerbound,
                                 [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}