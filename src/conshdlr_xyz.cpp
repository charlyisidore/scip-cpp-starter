/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "conshdlr_xyz.hpp"

const char *ConshdlrXyz::Properties::Name = "xyz";
const char *ConshdlrXyz::Properties::Desc = "constraint handler for xyz constraints";
const int ConshdlrXyz::Properties::Sepapriority = 0;
const int ConshdlrXyz::Properties::Enfopriority = 0;
const int ConshdlrXyz::Properties::Checkpriority = 0;
const int ConshdlrXyz::Properties::Sepafreq = -1;
const int ConshdlrXyz::Properties::Propfreq = -1;
const int ConshdlrXyz::Properties::Eagerfreq = 100;
const int ConshdlrXyz::Properties::Maxprerounds = -1;
const SCIP_Bool ConshdlrXyz::Properties::Delaysepa = false;
const SCIP_Bool ConshdlrXyz::Properties::Delayprop = false;
const SCIP_Bool ConshdlrXyz::Properties::Needscons = true;
const SCIP_PROPTIMING ConshdlrXyz::Properties::Proptiming = SCIP_PROPTIMING_BEFORELP;
const SCIP_PRESOLTIMING ConshdlrXyz::Properties::Presoltiming = SCIP_PRESOLTIMING_MEDIUM;

ConshdlrXyz::ConshdlrXyz(SCIP *scip)
    : scip::ObjConshdlr(scip,
                        Properties::Name,
                        Properties::Desc,
                        Properties::Sepapriority,
                        Properties::Enfopriority,
                        Properties::Checkpriority,
                        Properties::Sepafreq,
                        Properties::Propfreq,
                        Properties::Eagerfreq,
                        Properties::Maxprerounds,
                        Properties::Delaysepa,
                        Properties::Delayprop,
                        Properties::Needscons,
                        Properties::Proptiming,
                        Properties::Presoltiming)
{
}

SCIP_RETCODE ConshdlrXyz::scip_free(SCIP *scip, SCIP_CONSHDLR *conshdlr)
{
    return scip::ObjConshdlr::scip_free(scip, conshdlr);
}

SCIP_RETCODE ConshdlrXyz::scip_init(SCIP *scip,
                                    SCIP_CONSHDLR *conshdlr,
                                    SCIP_CONS **conss,
                                    int nconss)
{
    return scip::ObjConshdlr::scip_init(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_exit(SCIP *scip,
                                    SCIP_CONSHDLR *conshdlr,
                                    SCIP_CONS **conss,
                                    int nconss)
{
    return scip::ObjConshdlr::scip_exit(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_initpre(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss)
{
    return scip::ObjConshdlr::scip_initpre(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_exitpre(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss)
{
    return scip::ObjConshdlr::scip_exitpre(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_initsol(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss)
{
    return scip::ObjConshdlr::scip_initsol(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_exitsol(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss,
                                       SCIP_Bool restart)
{
    return scip::ObjConshdlr::scip_exitsol(scip,
                                           conshdlr,
                                           conss,
                                           nconss,
                                           restart);
}

SCIP_RETCODE ConshdlrXyz::scip_delete(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS *cons,
                                      SCIP_CONSDATA **consdata)
{
    return scip::ObjConshdlr::scip_delete(scip, conshdlr, cons, consdata);
}

SCIP_RETCODE ConshdlrXyz::scip_trans([[maybe_unused]] SCIP *scip,
                                     [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                     [[maybe_unused]] SCIP_CONS *sourcecons,
                                     [[maybe_unused]] SCIP_CONS **targetcons)
{
    return SCIP_OKAY;
}

SCIP_RETCODE ConshdlrXyz::scip_initlp(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS **conss,
                                      int nconss,
                                      SCIP_Bool *infeasible)
{
    return scip::ObjConshdlr::scip_initlp(scip,
                                          conshdlr,
                                          conss,
                                          nconss,
                                          infeasible);
}

SCIP_RETCODE ConshdlrXyz::scip_sepalp(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS **conss,
                                      int nconss,
                                      int nusefulconss,
                                      SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_sepalp(scip,
                                          conshdlr,
                                          conss,
                                          nconss,
                                          nusefulconss,
                                          result);
}

SCIP_RETCODE ConshdlrXyz::scip_sepasol(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss,
                                       int nusefulconss,
                                       SCIP_SOL *sol,
                                       SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_sepasol(scip,
                                           conshdlr,
                                           conss,
                                           nconss,
                                           nusefulconss,
                                           sol,
                                           result);
}

SCIP_RETCODE ConshdlrXyz::scip_enfolp([[maybe_unused]] SCIP *scip,
                                      [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                      [[maybe_unused]] SCIP_CONS **conss,
                                      [[maybe_unused]] int nconss,
                                      [[maybe_unused]] int nusefulconss,
                                      [[maybe_unused]] SCIP_Bool solinfeasible,
                                      [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}

SCIP_RETCODE ConshdlrXyz::scip_enforelax(SCIP *scip,
                                         SCIP_SOL *sol,
                                         SCIP_CONSHDLR *conshdlr,
                                         SCIP_CONS **conss,
                                         int nconss,
                                         int nusefulconss,
                                         SCIP_Bool solinfeasible,
                                         SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_enforelax(scip,
                                             sol,
                                             conshdlr,
                                             conss,
                                             nconss,
                                             nusefulconss,
                                             solinfeasible,
                                             result);
}

SCIP_RETCODE ConshdlrXyz::scip_enfops([[maybe_unused]] SCIP *scip,
                                      [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                      [[maybe_unused]] SCIP_CONS **conss,
                                      [[maybe_unused]] int nconss,
                                      [[maybe_unused]] int nusefulconss,
                                      [[maybe_unused]] SCIP_Bool solinfeasible,
                                      [[maybe_unused]] SCIP_Bool objinfeasible,
                                      [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}

SCIP_RETCODE ConshdlrXyz::scip_check([[maybe_unused]] SCIP *scip,
                                     [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                     [[maybe_unused]] SCIP_CONS **conss,
                                     [[maybe_unused]] int nconss,
                                     [[maybe_unused]] SCIP_SOL *sol,
                                     [[maybe_unused]] SCIP_Bool checkintegrality,
                                     [[maybe_unused]] SCIP_Bool checklprows,
                                     [[maybe_unused]] SCIP_Bool printreason,
                                     [[maybe_unused]] SCIP_Bool completely,
                                     [[maybe_unused]] SCIP_RESULT *result)
{
    return SCIP_OKAY;
}

SCIP_RETCODE ConshdlrXyz::scip_prop(SCIP *scip,
                                    SCIP_CONSHDLR *conshdlr,
                                    SCIP_CONS **conss,
                                    int nconss,
                                    int nusefulconss,
                                    int nmarkedconss,
                                    SCIP_PROPTIMING proptiming,
                                    SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_prop(scip,
                                        conshdlr,
                                        conss,
                                        nconss,
                                        nusefulconss,
                                        nmarkedconss,
                                        proptiming,
                                        result);
}

SCIP_RETCODE ConshdlrXyz::scip_presol(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS **conss,
                                      int nconss,
                                      int nrounds,
                                      SCIP_PRESOLTIMING presoltiming,
                                      int nnewfixedvars,
                                      int nnewaggrvars,
                                      int nnewchgvartypes,
                                      int nnewchgbds,
                                      int nnewholes,
                                      int nnewdelconss,
                                      int nnewaddconss,
                                      int nnewupgdconss,
                                      int nnewchgcoefs,
                                      int nnewchgsides,
                                      int *nfixedvars,
                                      int *naggrvars,
                                      int *nchgvartypes,
                                      int *nchgbds,
                                      int *naddholes,
                                      int *ndelconss,
                                      int *naddconss,
                                      int *nupgdconss,
                                      int *nchgcoefs,
                                      int *nchgsides,
                                      SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_presol(scip,
                                          conshdlr,
                                          conss,
                                          nconss,
                                          nrounds,
                                          presoltiming,
                                          nnewfixedvars,
                                          nnewaggrvars,
                                          nnewchgvartypes,
                                          nnewchgbds,
                                          nnewholes,
                                          nnewdelconss,
                                          nnewaddconss,
                                          nnewupgdconss,
                                          nnewchgcoefs,
                                          nnewchgsides,
                                          nfixedvars,
                                          naggrvars,
                                          nchgvartypes,
                                          nchgbds,
                                          naddholes,
                                          ndelconss,
                                          naddconss,
                                          nupgdconss,
                                          nchgcoefs,
                                          nchgsides,
                                          result);
}

SCIP_RETCODE ConshdlrXyz::scip_resprop(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS *cons,
                                       SCIP_VAR *infervar,
                                       int inferinfo,
                                       SCIP_BOUNDTYPE boundtype,
                                       SCIP_BDCHGIDX *bdchgidx,
                                       SCIP_Real relaxedbd,
                                       SCIP_RESULT *result)
{
    return scip::ObjConshdlr::scip_resprop(scip,
                                           conshdlr,
                                           cons,
                                           infervar,
                                           inferinfo,
                                           boundtype,
                                           bdchgidx,
                                           relaxedbd,
                                           result);
}

SCIP_RETCODE ConshdlrXyz::scip_lock([[maybe_unused]] SCIP *scip,
                                    [[maybe_unused]] SCIP_CONSHDLR *conshdlr,
                                    [[maybe_unused]] SCIP_CONS *cons,
                                    [[maybe_unused]] SCIP_LOCKTYPE locktype,
                                    [[maybe_unused]] int nlockspos,
                                    [[maybe_unused]] int nlocksneg)
{
    return SCIP_OKAY;
}

SCIP_RETCODE ConshdlrXyz::scip_active(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS *cons)
{
    return scip::ObjConshdlr::scip_active(scip, conshdlr, cons);
}

SCIP_RETCODE ConshdlrXyz::scip_deactive(SCIP *scip,
                                        SCIP_CONSHDLR *conshdlr,
                                        SCIP_CONS *cons)
{
    return scip::ObjConshdlr::scip_deactive(scip, conshdlr, cons);
}

SCIP_RETCODE ConshdlrXyz::scip_enable(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS *cons)
{
    return scip::ObjConshdlr::scip_enable(scip, conshdlr, cons);
}

SCIP_RETCODE ConshdlrXyz::scip_disable(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS *cons)
{
    return scip::ObjConshdlr::scip_disable(scip, conshdlr, cons);
}

SCIP_RETCODE ConshdlrXyz::scip_delvars(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS **conss,
                                       int nconss)
{
    return scip::ObjConshdlr::scip_delvars(scip, conshdlr, conss, nconss);
}

SCIP_RETCODE ConshdlrXyz::scip_print(SCIP *scip,
                                     SCIP_CONSHDLR *conshdlr,
                                     SCIP_CONS *cons,
                                     FILE *file)
{
    return scip::ObjConshdlr::scip_print(scip, conshdlr, cons, file);
}

SCIP_RETCODE ConshdlrXyz::scip_copy(SCIP *scip,
                                    SCIP_CONS **cons,
                                    const char *name,
                                    SCIP *sourcescip,
                                    SCIP_CONSHDLR *sourceconshdlr,
                                    SCIP_CONS *sourcecons,
                                    SCIP_HASHMAP *varmap,
                                    SCIP_HASHMAP *consmap,
                                    SCIP_Bool initial,
                                    SCIP_Bool separate,
                                    SCIP_Bool enforce,
                                    SCIP_Bool check,
                                    SCIP_Bool propagate,
                                    SCIP_Bool local,
                                    SCIP_Bool modifiable,
                                    SCIP_Bool dynamic,
                                    SCIP_Bool removable,
                                    SCIP_Bool stickingatnode,
                                    SCIP_Bool global,
                                    SCIP_Bool *valid)
{
    return scip::ObjConshdlr::scip_copy(scip,
                                        cons,
                                        name,
                                        sourcescip,
                                        sourceconshdlr,
                                        sourcecons,
                                        varmap,
                                        consmap,
                                        initial,
                                        separate,
                                        enforce,
                                        check,
                                        propagate,
                                        local,
                                        modifiable,
                                        dynamic,
                                        removable,
                                        stickingatnode,
                                        global,
                                        valid);
}

SCIP_RETCODE ConshdlrXyz::scip_parse(SCIP *scip,
                                     SCIP_CONSHDLR *conshdlr,
                                     SCIP_CONS **cons,
                                     const char *name,
                                     const char *str,
                                     SCIP_Bool initial,
                                     SCIP_Bool separate,
                                     SCIP_Bool enforce,
                                     SCIP_Bool check,
                                     SCIP_Bool propagate,
                                     SCIP_Bool local,
                                     SCIP_Bool modifiable,
                                     SCIP_Bool dynamic,
                                     SCIP_Bool removable,
                                     SCIP_Bool stickingatnode,
                                     SCIP_Bool *success)
{
    return scip::ObjConshdlr::scip_parse(scip,
                                         conshdlr,
                                         cons,
                                         name,
                                         str,
                                         initial,
                                         separate,
                                         enforce,
                                         check,
                                         propagate,
                                         local,
                                         modifiable,
                                         dynamic,
                                         removable,
                                         stickingatnode,
                                         success);
}

SCIP_RETCODE ConshdlrXyz::scip_getvars(SCIP *scip,
                                       SCIP_CONSHDLR *conshdlr,
                                       SCIP_CONS *cons,
                                       SCIP_VAR **vars,
                                       int varssize,
                                       SCIP_Bool *success)
{
    return scip::ObjConshdlr::scip_getvars(scip,
                                           conshdlr,
                                           cons,
                                           vars,
                                           varssize,
                                           success);
}

SCIP_RETCODE ConshdlrXyz::scip_getnvars(SCIP *scip,
                                        SCIP_CONSHDLR *conshdlr,
                                        SCIP_CONS *cons,
                                        int *nvars,
                                        SCIP_Bool *success)
{
    return scip::ObjConshdlr::scip_getnvars(scip,
                                            conshdlr,
                                            cons,
                                            nvars,
                                            success);
}

SCIP_RETCODE ConshdlrXyz::scip_getdivebdchgs(SCIP *scip,
                                             SCIP_CONSHDLR *conshdlr,
                                             SCIP_DIVESET *diveset,
                                             SCIP_SOL *sol,
                                             SCIP_Bool *success,
                                             SCIP_Bool *infeasible)
{
    return scip::ObjConshdlr::scip_getdivebdchgs(scip,
                                                 conshdlr,
                                                 diveset,
                                                 sol,
                                                 success,
                                                 infeasible);
}

SCIP_RETCODE ConshdlrXyz::scip_getpermsymgraph(SCIP *scip,
                                               SCIP_CONSHDLR *conshdlr,
                                               SCIP_CONS *cons,
                                               SYM_GRAPH *graph,
                                               SCIP_Bool *success)
{
    return scip::ObjConshdlr::scip_getpermsymgraph(scip,
                                                   conshdlr,
                                                   cons,
                                                   graph,
                                                   success);
}

SCIP_RETCODE ConshdlrXyz::scip_getsignedpermsymgraph(SCIP *scip,
                                                     SCIP_CONSHDLR *conshdlr,
                                                     SCIP_CONS *cons,
                                                     SYM_GRAPH *graph,
                                                     SCIP_Bool *success)
{
    return scip::ObjConshdlr::scip_getsignedpermsymgraph(scip,
                                                         conshdlr,
                                                         cons,
                                                         graph,
                                                         success);
}
