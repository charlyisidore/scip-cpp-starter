/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef EVENTHDLR_XYZ_HPP
#define EVENTHDLR_XYZ_HPP

#include <objscip/objeventhdlr.h>

/**
 * Event handler for xyz event.
 */
class EventhdlrXyz : public scip::ObjEventhdlr
{
public:
    /**
     * Event handler properties.
     */
    struct Properties
    {
        /**
         * Name of event handler.
         */
        static const char *Name;

        /**
         * Description of event handler.
         */
        static const char *Desc;
    };

    /**
     * Default constructor.
     */
    EventhdlrXyz(SCIP *scip);

    /**
     * Destructor of event handler to free user data (called when SCIP is
     * exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_EVENTHDLR *eventhdlr) override;

    /**
     * Initialization method of event handler (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_EVENTHDLR *eventhdlr) override;

    /**
     * Deinitialization method of event handler (called before transformed
     * problem is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_EVENTHDLR *eventhdlr) override;

    /**
     * Solving process initialization method of event handler (called when
     * branch and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip, SCIP_EVENTHDLR *eventhdlr) override;

    /**
     * Solving process deinitialization method of event handler (called before
     * branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_EVENTHDLR *eventhdlr) override;

    /**
     * Frees specific constraint data.
     */
    SCIP_RETCODE scip_delete(SCIP *scip,
                             SCIP_EVENTHDLR *eventhdlr,
                             SCIP_EVENTDATA **eventdata) override;

    /**
     * Execution method of event handler.
     */
    SCIP_RETCODE scip_exec(SCIP *scip,
                           SCIP_EVENTHDLR *eventhdlr,
                           SCIP_EVENT *event,
                           SCIP_EVENTDATA *eventdata) override;
};

#endif