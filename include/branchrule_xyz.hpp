/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BRANCHRULE_XYZ_HPP
#define BRANCHRULE_XYZ_HPP

#include <objscip/objbranchrule.h>

/**
 * xyz branching rule.
 */
class BranchruleXyz : public scip::ObjBranchrule
{
public:
    /**
     * Branching rule properties.
     */
    struct Properties
    {
        /**
         * Name of branching rule.
         */
        static const char *Name;

        /**
         * Description of branching rule.
         */
        static const char *Desc;

        /**
         * Priority of branching rule.
         */
        static const int Priority;

        /**
         * Maximal level depth.
         */
        static const int Maxdepth;

        /**
         * Maximal relative distance between dual bounds.
         */
        static const SCIP_Real Maxbounddist;
    };

    /**
     * Default constructor.
     */
    BranchruleXyz(SCIP *scip);

    /**
     * Destructor of branching rule to free user data (called when SCIP is
     * exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_BRANCHRULE *branchrule) override;

    /**
     * Initialization method of branching rule (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_BRANCHRULE *branchrule) override;

    /**
     * Deinitialization method of branching rule (called before transformed
     * problem is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_BRANCHRULE *branchrule) override;

    /**
     * Solving process initialization method of branching rule (called when
     * branch and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip, SCIP_BRANCHRULE *branchrule) override;

    /**
     * Solving process deinitialization method of branching rule (called before
     * branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_BRANCHRULE *branchrule) override;

    /**
     * Branching execution method for fractional LP solutions.
     */
    SCIP_RETCODE scip_execlp(SCIP *scip,
                             SCIP_BRANCHRULE *branchrule,
                             SCIP_Bool allowaddcons,
                             SCIP_RESULT *result) override;

    /**
     * Branching execution method for external candidates.
     */
    SCIP_RETCODE scip_execext(SCIP *scip,
                              SCIP_BRANCHRULE *branchrule,
                              SCIP_Bool allowaddcons,
                              SCIP_RESULT *result) override;

    /**
     * Branching execution method for not completely fixed pseudo solutions.
     */
    SCIP_RETCODE scip_execps(SCIP *scip,
                             SCIP_BRANCHRULE *branchrule,
                             SCIP_Bool allowaddcons,
                             SCIP_RESULT *result) override;
};

#endif