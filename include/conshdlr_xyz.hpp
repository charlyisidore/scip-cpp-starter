/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CONSHDLR_XYZ_HPP
#define CONSHDLR_XYZ_HPP

#include <objscip/objconshdlr.h>

/**
 * Constraint handler for xyz constraints.
 */
class ConshdlrXyz : public scip::ObjConshdlr
{
public:
    /**
     * Constraint handler properties.
     */
    struct Properties
    {
        /**
         * Name of constraint handler.
         */
        static const char *Name;

        /**
         * Description of constraint handler.
         */
        static const char *Desc;

        /**
         * Priority of constraint handler for separation.
         */
        static const int Sepapriority;

        /**
         * Priority of constraint handler for constraint enforcing.
         */
        static const int Enfopriority;

        /**
         * Priority of constraint handler for checking infeasibility (and
         * propagation).
         */
        static const int Checkpriority;

        /**
         * Frequency for separating cuts.
         */
        static const int Sepafreq;

        /**
         * Frequency for propagating domains.
         */
        static const int Propfreq;

        /**
         * Frequency for using all instead of only the useful constraints in
         * separation, propagation and enforcement.
         */
        static const int Eagerfreq;

        /**
         * Maximal number of presolving rounds the constraint handler participates
         * in.
         */
        static const int Maxprerounds;

        /**
         * Should separation method be delayed, if other separators found cuts?
         */
        static const SCIP_Bool Delaysepa;

        /**
         * Should propagation method be delayed, if other propagators found
         * reductions?
         */
        static const SCIP_Bool Delayprop;

        /**
         * Should the constraint handler be skipped, if no constraints are
         * available?
         */
        static const SCIP_Bool Needscons;

        /**
         * Positions in the node solving loop where propagation method of constraint
         * handlers should be executed.
         */
        static const SCIP_PROPTIMING Proptiming;

        /**
         * Timing mask of the constraint handler's presolving method.
         */
        static const SCIP_PRESOLTIMING Presoltiming;
    };

    /**
     * Default constructor.
     */
    ConshdlrXyz(SCIP *scip);

    /**
     * Destructor of constraint handler to free user data (called when SCIP is
     * exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_CONSHDLR *conshdlr) override;

    /**
     * Initialization method of constraint handler (called after problem has
     * been transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip,
                           SCIP_CONSHDLR *conshdlr,
                           SCIP_CONS **conss,
                           int nconss) override;

    /**
     * Deinitialization method of constraint handler (called before transformed
     * problem is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip,
                           SCIP_CONSHDLR *conshdlr,
                           SCIP_CONS **conss,
                           int nconss) override;

    /**
     * Presolving initialization method of constraint handler (called when
     * presolving is about to begin).
     */
    SCIP_RETCODE scip_initpre(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss) override;

    /**
     * Presolving deinitialization method of constraint handler (called after
     * presolving has been finished).
     */
    SCIP_RETCODE scip_exitpre(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss) override;

    /**
     * Solving process initialization method of constraint handler (called when
     * branch and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss) override;

    /**
     * Solving process deinitialization method of constraint handler (called
     * before branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss,
                              SCIP_Bool restart) override;

    /**
     * Frees specific constraint data.
     */
    SCIP_RETCODE scip_delete(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS *cons,
                             SCIP_CONSDATA **consdata) override;

    /**
     * Transforms constraint data into data belonging to the transformed
     * problem.
     */
    SCIP_RETCODE scip_trans(SCIP *scip,
                            SCIP_CONSHDLR *conshdlr,
                            SCIP_CONS *sourcecons,
                            SCIP_CONS **targetcons) override;

    /**
     * LP initialization method of constraint handler (called before the initial
     * LP relaxation at a node is solved).
     */
    SCIP_RETCODE scip_initlp(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS **conss,
                             int nconss,
                             SCIP_Bool *infeasible) override;

    /**
     * Separation method of constraint handler for LP solution.
     */
    SCIP_RETCODE scip_sepalp(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS **conss,
                             int nconss,
                             int nusefulconss,
                             SCIP_RESULT *result) override;

    /**
     * Separation method of constraint handler for arbitrary primal solution.
     */
    SCIP_RETCODE scip_sepasol(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss,
                              int nusefulconss,
                              SCIP_SOL *sol,
                              SCIP_RESULT *result) override;

    /**
     * Constraint enforcing method of constraint handler for LP solutions.
     */
    SCIP_RETCODE scip_enfolp(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS **conss,
                             int nconss,
                             int nusefulconss,
                             SCIP_Bool solinfeasible,
                             SCIP_RESULT *result) override;

    /**
     * Constraint enforcing method of constraint handler for relaxation
     * solutions.
     */
    SCIP_RETCODE scip_enforelax(SCIP *scip,
                                SCIP_SOL *sol,
                                SCIP_CONSHDLR *conshdlr,
                                SCIP_CONS **conss,
                                int nconss,
                                int nusefulconss,
                                SCIP_Bool solinfeasible,
                                SCIP_RESULT *result) override;

    /**
     * Constraint enforcing method of constraint handler for pseudo solutions.
     */
    SCIP_RETCODE scip_enfops(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS **conss,
                             int nconss,
                             int nusefulconss,
                             SCIP_Bool solinfeasible,
                             SCIP_Bool objinfeasible,
                             SCIP_RESULT *result) override;

    /**
     * Feasibility check method of constraint handler for primal solutions.
     */
    SCIP_RETCODE scip_check(SCIP *scip,
                            SCIP_CONSHDLR *conshdlr,
                            SCIP_CONS **conss,
                            int nconss,
                            SCIP_SOL *sol,
                            SCIP_Bool checkintegrality,
                            SCIP_Bool checklprows,
                            SCIP_Bool printreason,
                            SCIP_Bool completely,
                            SCIP_RESULT *result) override;

    /**
     * Domain propagation method of constraint handler.
     */
    SCIP_RETCODE scip_prop(SCIP *scip,
                           SCIP_CONSHDLR *conshdlr,
                           SCIP_CONS **conss,
                           int nconss,
                           int nusefulconss,
                           int nmarkedconss,
                           SCIP_PROPTIMING proptiming,
                           SCIP_RESULT *result) override;

    /**
     * Presolving method of constraint handler.
     */
    SCIP_RETCODE scip_presol(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS **conss,
                             int nconss,
                             int nrounds,
                             SCIP_PRESOLTIMING presoltiming,
                             int nnewfixedvars,
                             int nnewaggrvars,
                             int nnewchgvartypes,
                             int nnewchgbds,
                             int nnewholes,
                             int nnewdelconss,
                             int nnewaddconss,
                             int nnewupgdconss,
                             int nnewchgcoefs,
                             int nnewchgsides,
                             int *nfixedvars,
                             int *naggrvars,
                             int *nchgvartypes,
                             int *nchgbds,
                             int *naddholes,
                             int *ndelconss,
                             int *naddconss,
                             int *nupgdconss,
                             int *nchgcoefs,
                             int *nchgsides,
                             SCIP_RESULT *result) override;

    /**
     * Propagation conflict resolving method of constraint handler.
     */
    SCIP_RETCODE scip_resprop(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS *cons,
                              SCIP_VAR *infervar,
                              int inferinfo,
                              SCIP_BOUNDTYPE boundtype,
                              SCIP_BDCHGIDX *bdchgidx,
                              SCIP_Real relaxedbd,
                              SCIP_RESULT *result) override;

    /**
     * Variable rounding lock method of constraint handler.
     */
    SCIP_RETCODE scip_lock(SCIP *scip,
                           SCIP_CONSHDLR *conshdlr,
                           SCIP_CONS *cons,
                           SCIP_LOCKTYPE locktype,
                           int nlockspos,
                           int nlocksneg) override;

    /**
     * Constraint activation notification method of constraint handler.
     */
    SCIP_RETCODE scip_active(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS *cons) override;

    /**
     * Constraint deactivation notification method of constraint handler.
     */
    SCIP_RETCODE scip_deactive(SCIP *scip,
                               SCIP_CONSHDLR *conshdlr,
                               SCIP_CONS *cons) override;

    /**
     * Constraint enabling notification method of constraint handler.
     */
    SCIP_RETCODE scip_enable(SCIP *scip,
                             SCIP_CONSHDLR *conshdlr,
                             SCIP_CONS *cons) override;

    /**
     * Constraint disabling notification method of constraint handler.
     */
    SCIP_RETCODE scip_disable(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS *cons) override;

    /**
     * Variable deletion method of constraint handler.
     */
    SCIP_RETCODE scip_delvars(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS **conss,
                              int nconss) override;

    /**
     * Constraint display method of constraint handler.
     */
    SCIP_RETCODE scip_print(SCIP *scip,
                            SCIP_CONSHDLR *conshdlr,
                            SCIP_CONS *cons,
                            FILE *file) override;

    /**
     * Constraint copying method of constraint handler.
     */
    SCIP_RETCODE scip_copy(SCIP *scip,
                           SCIP_CONS **cons,
                           const char *name,
                           SCIP *sourcescip,
                           SCIP_CONSHDLR *sourceconshdlr,
                           SCIP_CONS *sourcecons,
                           SCIP_HASHMAP *varmap,
                           SCIP_HASHMAP *consmap,
                           SCIP_Bool initial,
                           SCIP_Bool separate,
                           SCIP_Bool enforce,
                           SCIP_Bool check,
                           SCIP_Bool propagate,
                           SCIP_Bool local,
                           SCIP_Bool modifiable,
                           SCIP_Bool dynamic,
                           SCIP_Bool removable,
                           SCIP_Bool stickingatnode,
                           SCIP_Bool global,
                           SCIP_Bool *valid) override;

    /**
     * Constraint parsing method of constraint handler.
     */
    SCIP_RETCODE scip_parse(SCIP *scip,
                            SCIP_CONSHDLR *conshdlr,
                            SCIP_CONS **cons,
                            const char *name,
                            const char *str,
                            SCIP_Bool initial,
                            SCIP_Bool separate,
                            SCIP_Bool enforce,
                            SCIP_Bool check,
                            SCIP_Bool propagate,
                            SCIP_Bool local,
                            SCIP_Bool modifiable,
                            SCIP_Bool dynamic,
                            SCIP_Bool removable,
                            SCIP_Bool stickingatnode,
                            SCIP_Bool *success) override;

    /**
     * Constraint method of constraint handler which returns the variables (if
     * possible).
     */
    SCIP_RETCODE scip_getvars(SCIP *scip,
                              SCIP_CONSHDLR *conshdlr,
                              SCIP_CONS *cons,
                              SCIP_VAR **vars,
                              int varssize,
                              SCIP_Bool *success) override;

    /**
     * Constraint method of constraint handler which returns the number of
     * variables (if possible).
     */
    SCIP_RETCODE scip_getnvars(SCIP *scip,
                               SCIP_CONSHDLR *conshdlr,
                               SCIP_CONS *cons,
                               int *nvars,
                               SCIP_Bool *success) override;

    /**
     * Constraint handler method to suggest dive bound changes during the
     * generic diving algorithm.
     */
    SCIP_RETCODE scip_getdivebdchgs(SCIP *scip,
                                    SCIP_CONSHDLR *conshdlr,
                                    SCIP_DIVESET *diveset,
                                    SCIP_SOL *sol,
                                    SCIP_Bool *success,
                                    SCIP_Bool *infeasible) override;

    /**
     * Constraint handler method which returns the permutation symmetry
     * detection graph of a constraint (if possible).
     */
    SCIP_RETCODE scip_getpermsymgraph(SCIP *scip,
                                      SCIP_CONSHDLR *conshdlr,
                                      SCIP_CONS *cons,
                                      SYM_GRAPH *graph,
                                      SCIP_Bool *success) override;

    /**
     * Constraint handler method providing the signed permutation symmetry
     * detection graph of a constraint (if possible).
     */
    SCIP_RETCODE scip_getsignedpermsymgraph(SCIP *scip,
                                            SCIP_CONSHDLR *conshdlr,
                                            SCIP_CONS *cons,
                                            SYM_GRAPH *graph,
                                            SCIP_Bool *success) override;
};

#endif