/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef VARDATA_XYZ_HPP
#define VARDATA_XYZ_HPP

#include <objscip/objvardata.h>

/**
 * xyz variable data.
 */
class VardataXyz : public scip::ObjVardata
{
public:
    /**
     * Destructor of user variable data to free original user data (called when
     * original variable is freed).
     */
    SCIP_RETCODE scip_delorig(SCIP *scip, SCIP_VAR *var) override;

    /**
     * Creates user data of transformed variable by transforming the original
     * user variable data (called after variable was transformed).
     */
    SCIP_RETCODE scip_trans(SCIP *scip,
                            SCIP_VAR *var,
                            scip::ObjVardata **objvardata,
                            SCIP_Bool *deleteobject) override;

    /**
     * Destructor of user variable data to free transformed user data (called
     * when transformed variable is freed).
     */
    SCIP_RETCODE scip_deltrans(SCIP *scip, SCIP_VAR *var) override;

    /**
     * Copies variable data of source SCIP variable for the target SCIP
     * variable.
     */
    SCIP_RETCODE scip_copy(SCIP *scip,
                           SCIP *sourcescip,
                           SCIP_VAR *sourcevar,
                           SCIP_HASHMAP *varmap,
                           SCIP_HASHMAP *consmap,
                           SCIP_VAR *targetvar,
                           scip::ObjVardata **objvardata,
                           SCIP_RESULT *result) override;
};

#endif