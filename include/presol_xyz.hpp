/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PRESOL_XYZ_HPP
#define PRESOL_XYZ_HPP

#include <objscip/objpresol.h>

/**
 * xyz presolver.
 */
class PresolXyz : public scip::ObjPresol
{
public:
    /**
     * Presolver properties.
     */
    struct Properties
    {
        /**
         * Name of presolver.
         */
        static const char *Name;

        /**
         * Description of presolver.
         */
        static const char *Desc;

        /**
         * Priority of presolver.
         */
        static const int Priority;

        /**
         * Maximal number of presolving rounds the presolver participates in.
         */
        static const int Maxrounds;

        /**
         * Timing mask of the presolver.
         */
        static const SCIP_PRESOLTIMING Timing;
    };

    /**
     * Default constructor.
     */
    PresolXyz(SCIP *scip);

    /**
     * Destructor of presolver to free user data (called when SCIP is exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_PRESOL *presol) override;

    /**
     * Initialization method of presolver (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_PRESOL *presol) override;

    /**
     * Deinitialization method of presolver (called before transformed problem
     * is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_PRESOL *presol) override;

    /**
     * Presolving initialization method of presolver (called when presolving is
     * about to begin).
     */
    SCIP_RETCODE scip_initpre(SCIP *scip, SCIP_PRESOL *presol) override;

    /**
     * Presolving deinitialization method of presolver (called after presolving
     * has been finished).
     */
    SCIP_RETCODE scip_exitpre(SCIP *scip, SCIP_PRESOL *presol) override;

    /**
     * Execution method of presolver.
     */
    SCIP_RETCODE scip_exec(SCIP *scip,
                           SCIP_PRESOL *presol,
                           int nrounds,
                           SCIP_PRESOLTIMING presoltiming,
                           int nnewfixedvars,
                           int nnewaggrvars,
                           int nnewchgvartypes,
                           int nnewchgbds,
                           int nnewholes,
                           int nnewdelconss,
                           int nnewaddconss,
                           int nnewupgdconss,
                           int nnewchgcoefs,
                           int nnewchgsides,
                           int *nfixedvars,
                           int *naggrvars,
                           int *nchgvartypes,
                           int *nchgbds,
                           int *naddholes,
                           int *ndelconss,
                           int *naddconss,
                           int *nupgdconss,
                           int *nchgcoefs,
                           int *nchgsides,
                           SCIP_RESULT *result) override;
};

#endif