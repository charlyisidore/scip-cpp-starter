/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PROBDATA_XYZ_HPP
#define PROBDATA_XYZ_HPP

#include <objscip/objprobdata.h>

/**
 * xyz problem data.
 */
class ProbdataXyz : public scip::ObjProbData
{
public:
    /**
     * Destructor of user problem data to free original user data (called when
     * original problem is freed).
     */
    SCIP_RETCODE scip_delorig(SCIP *scip) override;

    /**
     * Creates user data of transformed problem by transforming the original
     * user problem data (called after problem was transformed).
     */
    SCIP_RETCODE scip_trans(SCIP *scip,
                            scip::ObjProbData **objprobdata,
                            SCIP_Bool *deleteobject) override;

    /**
     * Destructor of user problem data to free transformed user data (called
     * when transformed problem is freed).
     */
    SCIP_RETCODE scip_deltrans(SCIP *scip) override;

    /**
     * Solving process initialization method of transformed data (called before
     * the branch and bound process begins).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip) override;

    /**
     * Solving process deinitialization method of transformed data (called
     * before the branch and bound data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_Bool restart) override;

    /**
     * Copies user data of source SCIP for the target SCIP.
     */
    SCIP_RETCODE scip_copy(SCIP *scip,
                           SCIP *sourcescip,
                           SCIP_HASHMAP *varmap,
                           SCIP_HASHMAP *consmap,
                           scip::ObjProbData **objprobdata,
                           SCIP_Bool global,
                           SCIP_RESULT *result) override;
};

#endif