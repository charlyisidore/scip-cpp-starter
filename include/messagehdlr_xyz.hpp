/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef MESSAGEHDLR_XYZ_HPP
#define MESSAGEHDLR_XYZ_HPP

#include <objscip/objmessagehdlr.h>

/**
 * xyz message handler.
 */
class MessagehdlrXyz : public scip::ObjMessagehdlr
{
public:
    /**
     * Message handler properties.
     */
    struct Properties
    {
        /**
         * Should the output be buffered up to the next line?
         */
        static const SCIP_Bool Bufferedoutput;
    };

    /**
     * Default constructor.
     */
    MessagehdlrXyz();

    /**
     * Error message print method of message handler.
     */
    void scip_error(SCIP_MESSAGEHDLR *messagehdlr,
                    FILE *file,
                    const char *msg) override;

    /**
     * Warning message print method of message handler.
     */
    void scip_warning(SCIP_MESSAGEHDLR *messagehdlr,
                      FILE *file,
                      const char *msg) override;

    /**
     * Dialog message print method of message handler.
     */
    void scip_dialog(SCIP_MESSAGEHDLR *messagehdlr,
                     FILE *file,
                     const char *msg) override;

    /**
     * Info message print method of message handler.
     */
    void scip_info(SCIP_MESSAGEHDLR *messagehdlr,
                   FILE *file,
                   const char *msg) override;

    /**
     * Destructor of message handler to free message handler data.
     */
    SCIP_RETCODE scip_free(SCIP_MESSAGEHDLR *messagehdlr) override;
};

#endif