/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PRICER_XYZ_HPP
#define PRICER_XYZ_HPP

#include <objscip/objpricer.h>

/**
 * xyz variable pricer.
 */
class PricerXyz : public scip::ObjPricer
{
public:
    /**
     * Variable pricer properties.
     */
    struct Properties
    {
        /**
         * Name of variable pricer.
         */
        static const char *Name;

        /**
         * Description of variable pricer.
         */
        static const char *Desc;

        /**
         * Priority of variable pricer.
         */
        static const int Priority;

        /**
         * Delay the variable pricer?
         */
        static const SCIP_Bool Delay;
    };

    /**
     * Default constructor.
     */
    PricerXyz(SCIP *scip);

    /**
     * Destructor of variable pricer to free user data (called when SCIP is
     * exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_PRICER *pricer) override;

    /**
     * Initialization method of variable pricer (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_PRICER *pricer) override;

    /**
     * Deinitialization method of variable pricer (called before transformed
     * problem is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_PRICER *pricer) override;

    /**
     * Solving process initialization method of variable pricer (called when
     * branch and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip, SCIP_PRICER *pricer) override;

    /**
     * Solving process deinitialization method of variable pricer (called
     * before branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_PRICER *pricer) override;

    /**
     * Reduced cost pricing method of variable pricer for feasible LPs.
     */
    SCIP_RETCODE scip_redcost(SCIP *scip,
                              SCIP_PRICER *pricer,
                              SCIP_Real *lowerbound,
                              SCIP_Bool *stopearly,
                              SCIP_RESULT *result) override;

    /**
     * Farkas pricing method of variable pricer for infeasible LPs.
     */
    SCIP_RETCODE scip_farkas(SCIP *scip,
                             SCIP_PRICER *pricer,
                             SCIP_RESULT *result) override;
};

#endif