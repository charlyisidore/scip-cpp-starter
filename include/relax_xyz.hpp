/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef RELAX_XYZ_HPP
#define RELAX_XYZ_HPP

#include <objscip/objrelax.h>

/**
 * xyz relaxator.
 */
class RelaxXyz : public scip::ObjRelax
{
public:
    /**
     * Relaxator properties.
     */
    struct Properties
    {
        /**
         * Name of relaxator.
         */
        static const char *Name;

        /**
         * Description of relaxator.
         */
        static const char *Desc;

        /**
         * Priority of the relaxator.
         */
        static const int Priority;

        /**
         * Frequency for calling relaxator.
         */
        static const int Freq;

        /**
         * Does the relaxator contain all cuts in the LP?
         */
        static const SCIP_Bool Includeslp;
    };

    /**
     * Default constructor.
     */
    RelaxXyz(SCIP *scip);

    /**
     * Destructor of relaxator to free user data (called when SCIP is exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_RELAX *relax) override;

    /**
     * Initialization method of relaxator (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_RELAX *relax) override;

    /**
     * Deinitialization method of relaxator (called before transformed problem
     * is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_RELAX *relax) override;

    /**
     * Solving process initialization method of relaxator (called when branch
     * and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip, SCIP_RELAX *relax) override;

    /**
     * Solving process deinitialization method of relaxator (called before
     * branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_RELAX *relax) override;

    /**
     * Execution method of relaxator.
     */
    SCIP_RETCODE scip_exec(SCIP *scip,
                           SCIP_RELAX *relax,
                           SCIP_Real *lowerbound,
                           SCIP_RESULT *result) override;
};

#endif