/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef TABLE_XYZ_HPP
#define TABLE_XYZ_HPP

#include <objscip/objtable.h>

/**
 * xyz statistics table.
 */
class TableXyz : public scip::ObjTable
{
public:
    /**
     * Statistics table properties.
     */
    struct Properties
    {
        /**
         * Name of statistics table.
         */
        static const char *Name;

        /**
         * Description of statistics table.
         */
        static const char *Desc;

        /**
         * Priority of statistics table.
         */
        static const int Position;

        /**
         * Output of the statistics table is only printed from this stage
         * onwards.
         */
        static const SCIP_STAGE Earlieststage;
    };

    /**
     * Default constructor.
     */
    TableXyz(SCIP *scip);

    /**
     * Destructor of statistics table to free user data (called when SCIP is
     * exiting).
     */
    SCIP_RETCODE scip_free(SCIP *scip, SCIP_TABLE *table) override;

    /**
     * Initialization method of statistics table (called after problem was
     * transformed).
     */
    SCIP_RETCODE scip_init(SCIP *scip, SCIP_TABLE *table) override;

    /**
     * Deinitialization method of statistics table (called before transformed
     * problem is freed).
     */
    SCIP_RETCODE scip_exit(SCIP *scip, SCIP_TABLE *table) override;

    /**
     * Solving process initialization method of statistics table (called when
     * branch and bound process is about to begin).
     */
    SCIP_RETCODE scip_initsol(SCIP *scip, SCIP_TABLE *table) override;

    /**
     * Solving process deinitialization method of statistics table (called
     * before branch and bound process data is freed).
     */
    SCIP_RETCODE scip_exitsol(SCIP *scip, SCIP_TABLE *table) override;

    /**
     * Output method of statistics table to output file stream 'file'.
     */
    SCIP_RETCODE scip_output(SCIP *scip,
                             SCIP_TABLE *table,
                             FILE *file) override;
};

#endif