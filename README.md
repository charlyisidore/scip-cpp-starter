# SCIP C++ starter

A starter template for developing solvers using the C++ API of [SCIP](https://www.scipopt.org/).

## Background

This repository contains clean and empty classes to quickly start developing a solver using the SCIP library.
The program does actually not solve any problem.
Feel free to modify the source and implement the methods you need.

## Install

The [SCIP optimization suite](https://www.scipopt.org/) is required.

``` bash
mkdir build
cd build/
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

## Usage

```
./scip_cpp_starter [-l <logfile>] [-q] [-s <settings>] [-r <randseed>] [-f <problem>] [-b <batchfile>] [-c "command"]
  -v, --version : print version and build options
  -l <logfile>  : copy output into log file
  -q            : suppress screen messages
  -s <settings> : load parameter settings (.set) file
  -f <problem>  : load and solve problem file
  -o <primref> <dualref> : pass primal and dual objective reference values for validation at the end of the solve
  -b <batchfile>: load and execute dialog command batch file (can be used multiple times)
  -r <randseed> : nonnegative integer to be used as random seed. Has priority over random seed specified through parameter settings (.set) file
  -c "command"  : execute single line of dialog commands (can be used multiple times)
```

## License

This software is licensed under the GPLv3.
Please see the `LICENSE` file for further information.